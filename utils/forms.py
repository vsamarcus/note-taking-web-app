from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, SelectMultipleField, HiddenField
from flask_pagedown.fields import PageDownField
from wtforms import validators


class LoginForm(FlaskForm):
    username = StringField('Username*', [validators.DataRequired("Please enter \
      your name.")])
    password = PasswordField('Password*', [validators.DataRequired("Please enter \
      your password.")])
    submit = SubmitField('Login')


class SignUpForm(FlaskForm):
    username = StringField('Username*', [validators.DataRequired("Please enter \
      your username")])
    email = StringField('Email*', [validators.DataRequired("Please enter \
      your email"), validators.Email('Email format incorrect')])
    password = PasswordField('Password*', [validators.DataRequired("Please enter \
      your password"), validators.EqualTo('confirm_password', message='Passwords must match')])
    confirm_password = PasswordField('Confirm Password*', [validators.DataRequired("Confirm \
      your password")])
    submit = SubmitField('Signup')


class AddNoteForm(FlaskForm):
    note_id = HiddenField("Note ID:")
    note_title = StringField('Note Title:', [validators.DataRequired("Please enter \
      a note title.")])
    # note = TextAreaField('Note:', [validators.DataRequired("Please enter \
    #   your password.")], widget=TextArea())
    note = PageDownField('Your Note:')
    tags = SelectMultipleField('Note Tags:')
    submit = SubmitField('Add Note')


class AddTagForm(FlaskForm):
    tag = StringField('Enter tag:', [validators.DataRequired("Please enter \
        the tag")])
    submit = SubmitField('Add Tag')


class ChangeEmailForm(FlaskForm):
    email = StringField('Email*', [validators.DataRequired("Please enter \
      your email"), validators.Email('Email format incorrect')])
    submit = SubmitField('Update Email')


class ChangePasswordForm(FlaskForm):
    password = PasswordField('Password*', [validators.DataRequired("Please enter \
      your password"), validators.EqualTo('confirm_password', message='Passwords must match')])
    confirm_password = PasswordField('Confirm Password*', [validators.DataRequired("Confirm \
      your password")])
    submit = SubmitField('Update Password')
