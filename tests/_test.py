import requests

def test_status():
	response = requests.get("http://0.0.0.0:5000/")
	assert response.status_code == 200

def test_status_profile():
	response = requests.get("http://0.0.0.0:5000/profile")
	assert response.status_code == 200

def test_status_login():
	response = requests.get("http://0.0.0.0:5000/login/")
	assert response.status_code == 200

def test_status_signup():
	response = requests.get("http://0.0.0.0:5000/signup/")
	assert response.status_code == 200

def test_status_logout():
	response = requests.get("http://0.0.0.0:5000/logout/")
	assert response.status_code == 200

def test_status_notes_add():
	response = requests.get("http://0.0.0.0:5000/notes/add/")
	assert response.status_code == 200

def test_status_tags_add():
	response = requests.get("http://0.0.0.0:5000/tags/add/")
	assert response.status_code == 200

def test_status_tags():
	response = requests.get("http://0.0.0.0:5000/tags/")
	assert response.status_code == 200

def test_status_profile():
	response = requests.get("http://0.0.0.0:5000/profile/settings/")
	assert response.status_code == 200

def test_status_profile_email():
	response = requests.get("http://0.0.0.0:5000/profile/settings/change_email/")
	assert response.status_code == 200

def test_status_profile_password():
	response = requests.get("http://0.0.0.0:5000/profile/settings/change_password/")
	assert response.status_code == 200
