"""A dummy docstring."""
from app import create_app

APP = create_app()
"""A dummy docstring."""

if __name__ == "__main__":
    APP.run(port=5000, debug=True, host="0.0.0.0")
