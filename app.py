"""A dummy docstring."""
import os
from flask_restful import Api
from flask import Flask
from flask_pagedown import PageDown

API = Api()
PAGEDOWN = PageDown()


def create_app():
    """A dummy docstring."""
    app = Flask(__name__)
    app.config['SECRET_KEY'] = os.urandom(24)

    API.init_app(app)
    PAGEDOWN.init_app(app)

    from core import core # pylint: disable=C
    app.register_blueprint(core)
    return app
